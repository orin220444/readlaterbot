set dotenv-load := true
default:
	cargo build
release:
	cargo build --release
raspberry:
	cross build --release --target aarch64-unknown-linux-gnu
bump_deps:
	cargo update
clippy:
	cargo +nightly clippy -Z unstable-options --fix --allow-dirty
check:
	cargo check
fmt:
	cargo fmt
dotenv_linter:
	dotenv-linter fix
precommit: check fmt dotenv_linter clippy
