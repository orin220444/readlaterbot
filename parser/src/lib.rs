//use std::process::Stdio;
use std::time::Duration;

//use tokio::process::Command;
//use tokio::time::timeout;
//use wait_timeout::ChildExt;
use url::Url;
// pub async fn parse_old(url: &Url) -> Result<MercuryOutput, MercuryError> {
//     //let relative_path = env::var("MERCURY_PATH").context("Cannot find 'MERCURY_PATH' variable")?;
//     //let full_path = canonicalize(relative_path).context("failed to find path")?;
//     let process_timeout = Duration::from_secs(5);
//     let mut process = Command::new("mercury-parser")
//         .stdout(Stdio::piped())
//         .stderr(Stdio::piped())
//         .arg(&url.to_string())
//         .arg("--format=html")
//         .spawn()?;
//     let mut stdout = Vec::new();
//     let child_stdout = process.stdout.take();
//     tokio::io::copy(&mut child_stdout.unwrap(), &mut stdout).await?;

//     let exit_status = match timeout(process_timeout, process.wait()).await {
//         Ok(n) => n?,
//         Err(_) => {
//             process.kill().await?;
//             return Err(MercuryError::ProcessTimeout);
//         }
//     };
//     if exit_status.success() {
//         let json = stdout.as_slice();
//         Ok(serde_json::from_slice::<MercuryOutput>(json)?)
//     } else {
//         let mut stderr = vec![];
//         if let Some(mut reader) = process.stderr {
//             reader.read_to_end(&mut stderr).await?;
//         }
//         let stderr = String::from_utf8(stderr).unwrap_or_default();
//         Err(MercuryError::OtherError)
//     }
// }
use serde::Deserialize;
#[derive(Debug, Deserialize)]
pub struct MercuryOutput {
    pub title: Option<String>,
    pub content: Option<String>,
    pub author: Option<String>,
    pub date_published: Option<String>,
    pub lead_image_url: Option<Url>,
    pub next_page_url: Option<Url>,
    pub dek: Option<String>,
    pub url: Url,
    pub domain: Option<String>,
    pub excerpt: Option<String>,
    pub word_count: Option<i64>,
    pub direction: Option<String>,
    pub total_pages: Option<i64>,
    pub rendered_pages: Option<i64>,
}
use thiserror::Error;
#[derive(Debug, Error)]
pub enum MercuryError {
    #[error("Parameter url is invalid")]
    InvalidUrl,
    #[error("failed to start mercury process")]
    CommandError(#[from] std::io::Error),
    #[error("Could not parse stderr")]
    ParseResponse,
    #[error("Process timed out")]
    ProcessTimeout,
    // #[error("cannot deserialize json")]
    // SerdeError(#[from] serde_json::Error),
    #[error("cannot deserialize json")]
    SerdeError(),
    #[error("failed to start mercury process")]
    RequestError(#[from] reqwest::Error),
    #[error("failed to start mercury process")]
    ParserError(#[from] url::ParseError),
    #[error("mercury error: {0}")]
    MercuryError(String),
    #[error("")]
    OtherError,
}
#[derive(Debug, Deserialize)]
struct MercuryOutputError {
    error: bool,
    messages: String,
}

pub async fn parse(url: &Url) -> Result<MercuryOutput, MercuryError> {
    //let relative_path = env::var("MERCURY_PATH").context("Cannot find 'MERCURY_PATH' variable")?;
    //let full_path = canonicalize(relative_path).context("failed to find path")?;
    let _process_timeout = Duration::from_secs(5);
    let _params = &[("url", url.to_string())];
    let request_url = format!("http://mercury:3000/parser?url={}", url.as_str());
    //request_url.set_port(Some(3000)).map_err(|_| println!("set port error!"));
    println!("{:#?}", request_url);
    let output = reqwest::get(request_url).await?.text().await?;
    println!("{:?}", output);

    if let Ok(output) = serde_json::from_str::<MercuryOutput>(&output) {
        println!("{:?}", output);
        return Ok(output);
    }
    if let Ok(output_error) = serde_json::from_str::<MercuryOutputError>(&output) {
        println!("{:?}", output_error);
        return Err(MercuryError::MercuryError(output_error.messages));
    }

    Err(MercuryError::SerdeError())

    // println!("mercury: {:#?}", output);
    // let output = output?;
    //   output
}
