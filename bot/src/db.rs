use anyhow::{Context, Result};
use mongodb::{options::ClientOptions, Client, Database};
/// Connect to mongodb database
pub async fn connect_to_db() -> Result<Database> {
    let client_options = ClientOptions::parse(
        &std::env::var("MONGODB_URL")
            .context("No db url present!")
            .unwrap(),
    )
    .await?;
    let client = Client::with_options(client_options)
        .context("Err while creating mongo client")
        .unwrap();
    let db = client.database("ReadLaterBot");
    Ok(db)
}
