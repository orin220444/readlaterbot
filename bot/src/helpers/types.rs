use teloxide::adaptors::{DefaultParseMode, Throttle};

pub type Bot = Throttle<DefaultParseMode<teloxide::Bot>>;
