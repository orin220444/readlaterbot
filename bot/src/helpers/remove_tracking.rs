use std::borrow::Borrow;

use log::debug;
use url::Url;
pub fn remove_tracking_parameters<'a>(url: &mut Url) {
    let mut queries = Vec::new();
    for query_pair in url.query_pairs() {
        debug!("{:#?}", query_pair);
        match query_pair.0.borrow() {
            "adfrom" => {}
            "nx_source" => {}
            "cpid" => {}
            "asgtbndr" => {}
            "gucconter" => {}
            "guce_referrer" => {}
            "guce_referrer_sig" => {}
            "_openstat" => {}
            "action_ref_map" => {}
            "action_type_map" => {}
            "fb_action_ids" => {}
            "fb_action_types" => {}
            "fb_comment_id" => {}
            "fb_ref" => {}
            "fb_source" => {}
            "fbclid" => {}
            "xtor" => {}
            "utm_campaign" => {}
            "utm_channel" => {}
            "utm_cid" => {}
            "utm_content" => {}
            "utm_id" => {}
            "utm_medium" => {}
            "utm_name" => {}
            "utm_place" => {}
            "utm_pubreferrer" => {}
            "utm_reader" => {}
            "utm_referrer" => {}
            "utm_serial" => {}
            "utm_social" => {}
            "utm_social_type" => {}
            "utm_source" => {}
            "utm_swu" => {}
            "utm_term" => {}
            "utm_userid" => {}
            "utm_viz_id" => {}
            "utm_product" => {}
            "utm_campaignid" => {}
            "utm_ad" => {}
            "utm_brand" => {}
            "utm_emcid" => {}
            "utm_emmid" => {}
            "utm_umguk" => {}
            "gbraid" => {}
            "wbraid" => {}
            "gclsrc" => {}
            "gclid" => {}
            "dpg_source" => {}
            "dpg_medium" => {}
            "dpg_content" => {}
            "admitad_uid" => {}
            "gps_adid" => {}
            "unicorn_click_id" => {}
            "adjust_creative" => {}
            "adjust_tracker_limit" => {}
            "adjust_tracker" => {}
            "adjust_adgroup" => {}
            "adjust_campaign" => {}
            "bsft_clkid" => {}
            "bsft_eid" => {}
            "bsft_mid" => {}
            "bsft_uid" => {}
            "bsft_aaid" => {}
            "bsft_ek" => {}
            "mtm_campaign" => {}
            "mtm_cid" => {}
            "mtm_content" => {}
            "mtm_group" => {}
            "mtm_keyword" => {}
            "mtm_medium" => {}
            "mtm_placement" => {}
            "mtm_source" => {}
            "pk_campaign" => {}
            "pk_source" => {}
            "_branch_match_id" => {}
            "vc_lpp" => {}
            "ml_subscriber" => {}
            "ml_subscriber_hash" => {}
            "rb_clickid" => {}
            "oly_anon_id" => {}
            "ebisAdID" => {}
            "irgwc" => {}
            "fbclid" => {}
            _ => {
                let query = format!("{}={}", query_pair.0, query_pair.1);
                queries.push(query)
            }
        }
    }
    log::debug!("{}", queries.len());
    if queries.is_empty() {
        url.set_query(None)
    } else {
        let queries = queries.join("&");
        url.set_query(Some(&queries));
    }
}
