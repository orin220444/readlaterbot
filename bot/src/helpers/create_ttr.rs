use teloxide::utils::markdown::escape;
pub fn create_ttr(answer: &mut String, ttr: i32) {
    let mut ttr_text = format!(" ({} minutes)", ttr);
    ttr_text = escape(&ttr_text);
    answer.push_str(&ttr_text);
}
