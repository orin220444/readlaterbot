use teloxide::utils::markdown::{escape, link};
pub fn wrap_url(url: &str, title: &str) -> String {
    let title = escape(title);
    link(url, &title)
}
