use log::info;
use teloxide::types::{
    InlineKeyboardButtonKind, InlineKeyboardMarkup, MediaKind, Message, MessageEntity,
    MessageEntityKind, MessageKind,
};
use url::Url;
pub fn link_finder(message: &Message) -> Vec<Url> {
    let message_kind = &message.kind;
    let mut urls: Vec<Url> = Vec::new();
    parse_media_kind(message_kind, &mut urls);
    if let Some(keyboard) = message.reply_markup() {
        parse_keyboard(keyboard, &mut urls);
    }
    urls
}
fn entities_parse(entity: &MessageEntity, text: &String) -> Option<String> {
    let mut res = None;
    match &entity.kind {
        MessageEntityKind::TextLink { url } => res = Some(url.to_string()),
        MessageEntityKind::Url => {
            let url: String = {
                let text = text;
                let start = entity.offset;
                let length = entity.length;
                text.chars().skip(start).take(length).collect()
            };
            info!("{:#?}", url);
            res = Some(url);
        }
        _ => {}
    };
    res
}
fn parse_media_kind(message_kind: &MessageKind, urls: &mut Vec<Url>) {
    match message_kind {
        MessageKind::Common(message_common) => match &message_common.media_kind {
            MediaKind::Text(text_data) => {
                //let mut urls: Vec<String> = Vec::new();
                for entity in &text_data.entities {
                    let text = &text_data.text;
                    if let Some(url) = entities_parse(entity, text) {
                        if let Ok(url) = Url::parse(&url) {
                            urls.push(url);
                        }
                    }
                }
                Some(urls);
            }
            MediaKind::Photo(data) => {
                for entity in &data.caption_entities {
                    let caption = match &data.caption {
                        Some(str) => str.to_string(),
                        None => String::default(),
                    };
                    info!("{:#?}", caption);
                    if let Some(url) = entities_parse(entity, &caption) {
                        if let Ok(url) = Url::parse(&url) {
                            urls.push(url);
                        }
                    }
                }
            }
            _ => {}
        },
        _ => {}
    }
}
fn parse_keyboard(keyboard: &InlineKeyboardMarkup, urls: &mut Vec<Url>) {
    for row in &keyboard.inline_keyboard {
        for button in row {
            match &button.kind {
                InlineKeyboardButtonKind::Url(url) => urls.push(url.to_owned()),
                _ => {}
            }
        }
    }
}
