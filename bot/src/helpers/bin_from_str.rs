use anyhow::Result;
use mongodb::bson::{spec::BinarySubtype, Binary};
use uuid::Uuid;
/// Convert string to uuid
pub fn uuid_from_str(string: &str) -> Result<Binary> {
    let uuid = Uuid::parse_str(string)?;
    Ok(Binary {
        subtype: BinarySubtype::Uuid,
        bytes: uuid.as_bytes().to_vec(),
    })
}
