pub fn count_ttr(word_count: Option<i64>) -> Option<i32> {
    word_count.map(|word_count| (word_count / 150).try_into().unwrap())
}
