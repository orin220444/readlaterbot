use mongodb::bson::serde_helpers::uuid_as_binary;
use mongodb::bson::{self, doc};
use serde::{Deserialize, Serialize};
use teloxide::types::UserId;
use typed_builder::TypedBuilder;
use uuid::Uuid;
#[derive(Serialize, Deserialize, Debug, TypedBuilder)]
pub struct User {
    #[serde(with = "uuid_as_binary")]
    id: Uuid,
    pub telegram_id: UserId,
    pub telegram_username: Option<String>,
    pub joined: mongodb::bson::DateTime,
    pub show_real_url: bool,
}

#[derive(Serialize, Deserialize, Debug, TypedBuilder)]
pub struct UserPost {
    #[serde(with = "uuid_as_binary")]
    id: Uuid,
    #[serde(with = "uuid_as_binary")]
    post_id: Uuid,
    #[serde(with = "uuid_as_binary")]
    user_id: Uuid,
    original_url: String,
    read: bool,
    read_at: Option<bson::DateTime>,
    favorite: bool,
    show_real_url: bool,
}
impl UserPost {
    pub fn original_url(&self) -> String {
        self.original_url.clone()
    }
    pub fn show_real_url(&self) -> bool {
        self.show_real_url
    }

    pub fn post_id(&self) -> Uuid {
        self.post_id
    }
    pub fn read(&self) -> bool {
        self.read
    }
    pub fn favorite(&self) -> bool {
        self.favorite
    }
}
