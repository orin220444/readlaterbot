use chrono::{DateTime, Utc};
use mongodb::bson::{
    doc,
    serde_helpers::{chrono_datetime_as_bson_datetime, uuid_as_binary},
};
use serde::{Deserialize, Serialize};
use typed_builder::TypedBuilder;

use uuid::Uuid;
#[derive(Serialize, Deserialize, Debug, PartialEq, TypedBuilder)]
pub struct Post {
    #[serde(with = "uuid_as_binary")]
    id: Uuid,
    pub title: Option<String>,
    pub word_count: Option<i64>,
    pub real_url: String,
    #[serde(with = "chrono_datetime_as_bson_datetime")]
    pub created: DateTime<Utc>,
}

impl Post {
    pub fn id(&self) -> Uuid {
        self.id
    }
    pub fn title(&self) -> Option<String> {
        self.title.clone()
    }
}
