use teloxide::types::{InlineKeyboardButton, InlineKeyboardButtonKind, InlineKeyboardMarkup};
pub fn standart_keyboard(id: &str, is_archived: bool, is_favorite: bool) -> InlineKeyboardMarkup {
    let archive_button = {
        if is_archived {
            InlineKeyboardButton::new(
                "Unarchive",
                InlineKeyboardButtonKind::CallbackData(format!("unarchive {}", id)),
            )
        } else {
            InlineKeyboardButton::new(
                "Archive",
                InlineKeyboardButtonKind::CallbackData(format!("archive {}", id)),
            )
        }
    };
    let favorite_button = {
        if is_favorite {
            InlineKeyboardButton::new(
                "Unfavorite",
                InlineKeyboardButtonKind::CallbackData(format!("unfavorite {}", id)),
            )
        } else {
            InlineKeyboardButton::new(
                "Favorite",
                InlineKeyboardButtonKind::CallbackData(format!("favorite {}", id)),
            )
        }
    };
    InlineKeyboardMarkup::default().append_row(vec![
        InlineKeyboardButton::new(
            "Delete",
            InlineKeyboardButtonKind::CallbackData(format!("del {}", id)),
        ),
        archive_button,
        favorite_button,
    ])
}
