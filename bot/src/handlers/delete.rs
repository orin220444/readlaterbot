use crate::{db::connect_to_db, helpers::Bot};
use anyhow::Result;
use teloxide::prelude::*;
use uuid::Uuid;
pub async fn delete(bot: Bot, q: CallbackQuery, data: &str) -> Result<()> {
    let user_id = q.from.id;
    let user = get_user(user_id).await?;
    delete_post(data, user.id).await?;
    bot.answer_callback_query(q.id).text("Deleted!").await?;
    if let Some(message) = q.message {
        bot.delete_message(message.chat.id, message.id).await?;
    }
    Ok(())
}
use crate::helpers::uuid_from_str;
use mongodb::bson::doc;

use super::{get_user, UserPost};
async fn delete_post(post_id: &str, user_id: Uuid) -> Result<()> {
    let bin_id = uuid_from_str(post_id)?;
    let db = connect_to_db().await?;
    let coll = db.collection::<UserPost>("userposts");
    let query = doc! {
        "user_id": user_id,
        "post_id": &bin_id
    };
    coll.delete_one(query, None).await?;
    Ok(())
}
