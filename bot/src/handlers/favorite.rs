use super::get_user;
use crate::{helpers::Bot, keyboards::standart_keyboard, models::UserPost};
use anyhow::{Context, Result};
use teloxide::prelude::*;
use uuid::Uuid;

pub async fn favorite(bot: Bot, q: CallbackQuery, data: &str) -> Result<()> {
    let user_id = q.from.id;
    let user = get_user(user_id).await?;
    favorite_post(data, user.id).await?;
    let is_archived = is_archived(data, user.id).await?;
    let message_id = q.message.clone().unwrap().id;
    let chat_id = q.message.clone().unwrap().chat.id;
    bot.answer_callback_query(q.id).text("favorite!").await?;
    bot.edit_message_reply_markup(chat_id, message_id)
        .reply_markup(standart_keyboard(data, is_archived, true))
        .await?;
    Ok(())
}
use crate::{db::connect_to_db, helpers::uuid_from_str};
use mongodb::bson::doc;
use tokio_stream::StreamExt;
async fn favorite_post(post_id: &str, user_id: Uuid) -> Result<()> {
    let bin_id = uuid_from_str(post_id)?;
    let db = connect_to_db().await?;
    let coll = db.collection::<UserPost>("userposts");
    let query = doc! {
        "user_id": user_id,
        "post_id": &bin_id
    };
    let update = doc! {
        "$set": {
            "favorite": true,
        }
    };
    coll.update_one(query, update, None).await?;
    Ok(())
}
pub async fn is_archived(post_id: &str, user_id: Uuid) -> Result<bool> {
    let bin_id = uuid_from_str(post_id)?;
    let db = connect_to_db().await?;
    let coll = db.collection::<UserPost>("userposts");
    let match_aggr = doc! {
        "$match": {
        "user_id": user_id,
        "post_id": &bin_id
    }
    };
    let proj_aggr = doc! {
        "$project": {"read": 1_i32, "_id": 0_i32}
    };
    let pipeline = [match_aggr, proj_aggr];
    let res = coll
        .aggregate(pipeline, None)
        .await
        .context("test2232")?
        .next()
        .await
        .context("get is_archived error")??
        .get_bool("read")?;
    Ok(res)
}
