use crate::{helpers::Bot, keyboards::standart_keyboard, models::UserPost};
use anyhow::Result;
use teloxide::prelude::*;
use uuid::Uuid;

pub async fn archive(bot: Bot, q: CallbackQuery, data: &str) -> Result<()> {
    let user_id = q.from.id;
    let user = get_user(user_id).await?;
    archive_post(data, user.id).await?;
    let is_favorite = is_favorite(data, user.id).await?;
    let message_id = q.message.clone().unwrap().id;
    let chat_id = q.message.clone().unwrap().chat.id;
    bot.answer_callback_query(q.id).text("Archived!").await?;
    bot.edit_message_text(
        chat_id,
        message_id,
        format!("{} [Archived]", q.message.clone().unwrap().text().unwrap()),
    )
    .entities(q.message.clone().unwrap().entities().unwrap().to_owned()) //.map(|entities| entities.to_owned().into_iter()))
    .await?;
    bot.edit_message_reply_markup(chat_id, message_id)
        .reply_markup(standart_keyboard(data, true, is_favorite))
        //.parse_mode(MarkdownV2)
        .await?;
    Ok(())
}
use super::{get_user, is_favorite};
use crate::{db::connect_to_db, helpers::uuid_from_str};
use mongodb::bson::doc;
async fn archive_post(post_id: &str, user_id: Uuid) -> Result<()> {
    let bin_id = uuid_from_str(post_id)?;
    let db = connect_to_db().await?;
    let coll = db.collection::<UserPost>("userposts");
    let query = doc! {
        "user_id": user_id,
        "post_id": &bin_id
    };
    let update = doc! {
        "$set": {
            "read": true,
        }
    };
    coll.update_one(query, update, None).await?;
    Ok(())
}
