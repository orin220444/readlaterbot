use crate::helpers::{count_unread_posts, Bot};
use anyhow::{Context, Result};
use teloxide::prelude::*;

use super::get_user;
pub async fn count_unread_posts_handler(bot: Bot, m: Message) -> Result<()> {
    let user_id = m.from().context("NoneError")?.id;
    let user = get_user(user_id).await?;
    let unread_posts = count_unread_posts(user.id).await?;
    bot.send_message(user_id, format!("Unread posts: {}", unread_posts))
        .await?;
    Ok(())
}
