use crate::{handlers::get_user, helpers::Bot, keyboards::standart_keyboard};
use anyhow::{Context, Result};
use teloxide::prelude::*;
use uuid::Uuid;

pub async fn unarchive(bot: Bot, q: CallbackQuery, data: &str) -> Result<()> {
    let user_id = q.from.id;
    let user = get_user(user_id).await?;
    unarchive_post(data, user.id).await?;
    let is_favorite = is_favorite(data, user.id).await?;
    let message_id = q.message.clone().unwrap().id;
    let chat_id = q.message.clone().unwrap().chat.id;
    let text = clear_label(q.message.clone().unwrap().text().unwrap().to_string());
    bot.answer_callback_query(q.id).text("Unarchived!").await?;
    bot.edit_message_text(chat_id, message_id, text)
        .entities(q.message.clone().unwrap().entities().unwrap().to_owned())
        .await?;
    bot.edit_message_reply_markup(chat_id, message_id)
        .reply_markup(standart_keyboard(data, false, is_favorite))
        .await?;
    Ok(())
}

fn clear_label(text: String) -> String {
    if text.contains("[Archived]") {
        text.strip_suffix("[Archived]").unwrap().to_string()
    } else {
        text
    }
}
use crate::db::connect_to_db;
use crate::helpers::uuid_from_str;
use crate::models::UserPost;
use tokio_stream::StreamExt;

use mongodb::bson::doc;
async fn unarchive_post(post_id: &str, user_id: Uuid) -> Result<()> {
    println!("{:#?}", post_id);
    let bin_id = uuid_from_str(post_id)?;
    let db = connect_to_db().await?;
    let coll = db.collection::<UserPost>("userposts");
    println!("{:#?}", user_id);
    let query = doc! {
        "user_id": user_id,
        "post_id": &bin_id
    };
    let update = doc! {
        "$set": {
            "read": false
        }
    };
    coll.update_one(query, update, None).await?;
    Ok(())
}
pub async fn is_favorite(post_id: &str, user_id: Uuid) -> Result<bool> {
    let bin_id = uuid_from_str(post_id)?;
    let db = connect_to_db().await?;
    let coll = db.collection::<UserPost>("userposts");
    let match_aggr = doc! {
        "$match":{
        "user_id": user_id,
        "post_id": &bin_id
    }
    };
    let proj_aggr = doc! {
        "$project": {"favorite": 1_i32, "_id": 0_i32}
    };
    let pipeline = [match_aggr, proj_aggr];
    let res = coll
        .aggregate(pipeline, None)
        .await
        .context("test2232")?
        .next()
        .await
        .context("get is_favorite error")??
        .get_bool("favorite")?;
    Ok(res)
}
