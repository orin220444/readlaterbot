use crate::helpers::Bot;
use anyhow::{Context, Result};
use teloxide::prelude::*;
pub async fn start(_bot: Bot, m: Message) -> Result<()> {
    let telegram_user = m.from().context("NoneError")?;
    let user_id = telegram_user.id;
    let user_username = telegram_user.username.clone();
    db::create_or_find_user(user_id, user_username).await?;
    Ok(())
}

mod db {
    use crate::models::User;
    use anyhow::Result;
    use mongodb::bson::{doc, to_bson, to_document, Document};
    use std::ops::Not;
    use teloxide::types::UserId;

    pub async fn check_user_exists(user_id: UserId) -> Result<bool> {
        let db = crate::db::connect_to_db().await?;
        let user_id = to_bson(&user_id)?;
        let coll = db.collection::<Document>("users");
        let filter = doc! {
        "telegramId": user_id
        };
        let res = coll.count_documents(filter, None).await?.gt(&0);
        Ok(res)
    }
    pub async fn create_user(telegram_id: UserId, telegram_username: Option<String>) -> Result<()> {
        let joined = chrono::Utc::now().into();
        let id = uuid::Uuid::new_v4();
        let _show_real_url = false;
        let user = User::builder()
            .id(id)
            .telegram_id(telegram_id)
            .telegram_username(telegram_username)
            .joined(joined)
            .show_real_url(false)
            .build();
        let db = crate::db::connect_to_db().await?;
        let coll = db.collection::<Document>("users");
        coll.insert_one(to_document(&user)?, None).await?;
        Ok(())
    }
    pub async fn create_or_find_user(user_id: UserId, user_username: Option<String>) -> Result<()> {
        if check_user_exists(user_id).await?.not() {
            create_user(user_id, user_username).await?;
        }
        Ok(())
    }
}
