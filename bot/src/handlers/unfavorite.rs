use crate::{handlers::get_user, helpers::Bot, keyboards::standart_keyboard};
use anyhow::Result;
use teloxide::prelude::*;
use uuid::Uuid;

pub async fn unfavorite(bot: Bot, q: CallbackQuery, data: &str) -> Result<()> {
    let user_id = q.from.id;
    let user = get_user(user_id).await?;
    unfavorite_post(data, user.id).await?;
    let is_archived = is_archived(data, user.id).await?;
    let message_id = q.message.clone().unwrap().id;
    let chat_id = q.message.clone().unwrap().chat.id;
    bot.answer_callback_query(q.id).text("Unfavorite!").await?;
    /*bot.edit_message_text(chat_id, message_id, text)
    .entities(q.message.clone().unwrap().entities().unwrap().to_owned())
    .await?;*/
    bot.edit_message_reply_markup(chat_id, message_id)
        .reply_markup(standart_keyboard(data, is_archived, false))
        .await?;
    Ok(())
}

use crate::db::connect_to_db;
use crate::helpers::uuid_from_str;
use crate::models::UserPost;

use mongodb::bson::doc;

use super::is_archived;
async fn unfavorite_post(post_id: &str, user_id: Uuid) -> Result<()> {
    println!("{:#?}", post_id);
    let bin_id = uuid_from_str(post_id)?;
    let db = connect_to_db().await?;
    let coll = db.collection::<UserPost>("userposts");
    println!("{:#?}", user_id);
    let query = doc! {
        "user_id": user_id,
        "post_id": &bin_id
    };
    let update = doc! {
        "$set": {
            "favorite": false
        }
    };
    coll.update_one(query, update, None).await?;
    Ok(())
}
