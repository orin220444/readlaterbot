use crate::{
    helpers::{count_ttr, create_ttr, link_finder, remove_tracking_parameters, wrap_url, Bot},
    keyboards,
    models::{Post, User, UserPost},
};
use anyhow::{Context, Result};
use futures::StreamExt;
use log::info;
use mongodb::{
    bson::{doc, from_document, oid::ObjectId, to_bson, to_document},
    options::FindOneOptions,
    Database,
};

use serde::{Deserialize, Serialize};
use std::convert::TryInto;
use std::str::FromStr;
use teloxide::{
    prelude::*,
    types::{UserId},
    utils::markdown::{escape, escape_link_url},
};
/// Add new url to the db and belong it to user
pub async fn add(bot: Bot, m: Message) -> Result<()> {
    let urls = link_finder(&m);
    if urls.is_empty() {
        info!("No urls in the message!");
    }
    info!("Urls in the message: {:#?}", urls);
    let db = connect_to_db()
        .await
        .context("Error while connecting to db:")?;
    let telegram_user = m.from().context("Error while get telegram user data:")?;
    let user_id = telegram_user.id;
    let user_username = telegram_user.username.clone();
    let user_id = {
        if let Ok(user_id) = user_exists(user_id, &db).await {
            user_id
        } else {
            create_user(user_id, user_username, &db).await?
        }
    };
    for mut url in urls {
        remove_tracking_parameters(&mut url);
        let mut real_url = real_url(&url).await;
        remove_tracking_parameters(&mut real_url);
        // If post exists, see if it belongs to the user.
        if let Ok(post_id) = post_exists(&real_url, &db).await {
            if let Some(user_post_info) =
                check_duplicates_in_post_map(user_id, post_id, &db, &url).await?
            {
                bot.send_message(telegram_user.id, url.to_string())
                    .reply_markup(keyboards::standart_keyboard(
                        &post_id.to_hyphenated().to_string(),
                        user_post_info.read,
                        user_post_info.favorite,
                    ))
                    .await?;
            }
            // If post does not exist, create it
        } else {
            let post = create_post(real_url).await?;
            save_post_to_db(&post, &db).await?;
            append_post_to_user(user_id, post.id(), &db, url.as_str()).await?;
            let _post_id = post.id();

            log::info!("Successful saved post");
            let mut answer = {
                let url_str = url.as_str();
                if let Some(title) = post.title() {
                    wrap_url(url_str, &title)
                } else {
                    escape(&escape_link_url(url_str))
                }
            };
            let ttr = count_ttr(post.word_count);
            if let Some(ttr) = ttr {
                create_ttr(&mut answer, ttr)
            };
            bot.send_message(telegram_user.id, answer)
                .reply_markup(keyboards::standart_keyboard(
                    &post.id().to_hyphenated().to_string(),
                    false,
                    false,
                ))
                //   .parse_mode(ParseMode::MarkdownV2)
                .await?;
        }
    }

    Ok(())
}
use url::Url;
// Open web page using reqwest and get final url
async fn real_url(original_url: &Url) -> Url {
    match reqwest::get(original_url.to_owned()).await {
        Err(e) => {
            info!("{:#?}", e);
            original_url.to_owned()
        }
        Ok(res) => {
            //TODO: work with url
            let real_url = res.url().to_owned();
            info!("{}", real_url);
            real_url
        }
    }
}
use chrono::{DateTime, Utc};
use uuid::Uuid;
fn created() -> DateTime<Utc> {
    Utc::now()
}
use crate::db::connect_to_db;
use mongodb::bson::Document;

async fn post_exists(post_url: &Url, db: &Database) -> Result<Uuid> {
    let coll = db.collection::<Post>("posts");
    let match_aggr = doc! {
    "$match":{
        "real_url": post_url.to_string()
    }
    };
    let proj_aggr = doc! {
    "$project":{
    "id":1_i32,
    "_id":0_i32,
    }
    };
    let pipeline = [match_aggr, proj_aggr];
    let vec_res = coll
        .aggregate(pipeline, None)
        .await?
        .next()
        .await
        .context("NoneError")??
        .get_binary_generic("id")?
        .to_owned()
        .as_slice()
        .try_into()
        .context("error while trying to convert slice to uuid")?;
    println!("{:#?}", vec_res);
    let res = Uuid::from_bytes(vec_res);
    Ok(res)
}
use mongodb::bson::{spec::BinarySubtype, Binary};
async fn check_duplicates_in_post_map(
    user_id: Uuid,
    post_id: Uuid,
    db: &Database,
    _original_url: &Url,
) -> Result<Option<UserPostInfo>> {
    let coll = db.collection::<Document>("userposts");
    let bin_id = Binary {
        subtype: BinarySubtype::Uuid,
        bytes: post_id.as_bytes().to_vec(),
    };
    let match1_aggr = doc! {
    "$match": {
        "user_id": user_id,
        "post_id": bin_id,
    }
    };
    let proj_aggr = doc! {
        "$project":{
            "read": 1_i32,
            "favorite": 1_i32,
            "_id":0_i32
        }
    };
    let pipeline = [match1_aggr, proj_aggr];
    let res = coll
        .aggregate(pipeline, None)
        .await?
        .next()
        .await
        .and_then(|res| from_document::<UserPostInfo>(res.unwrap()).ok()); //.context("duplicate find error")??;
    Ok(res)
}
#[derive(Debug, Serialize, Deserialize)]
pub struct UserPostInfo {
    pub read: bool,
    pub favorite: bool,
}
async fn append_post_to_user(
    user_id: Uuid,
    post_id: Uuid,
    db: &Database,
    original_url: &str,
) -> Result<()> {
    let coll = db.collection::<UserPost>("userposts");
    let post_builder = UserPost::builder()
        .id(Uuid::new_v4())
        .user_id(user_id)
        .original_url(original_url.to_string())
        .favorite(false)
        .post_id(post_id)
        .read(false)
        .show_real_url(false)
        .read_at(None)
        .build();
    coll.insert_one(post_builder, None).await?;
    Ok(())
}
async fn save_post_to_db(post: &Post, db: &Database) -> Result<()> {
    let coll = db.collection::<Post>("posts");
    coll.insert_one(post, None).await?;
    Ok(())
}
async fn get_post_id(real_url: &str, db: &Database) -> Result<Uuid> {
    let coll = db.collection::<Document>("posts");
    let filter = doc! {
        "real_url": real_url
    };
    let projection = doc! {
        "id": 1
    };
    let options = FindOneOptions::builder().projection(projection).build();
    let doc = coll.find_one(filter, options).await?.context("NoneError")?;
    let res = doc.get_binary_generic("id")?.clone();
    let res = Uuid::from_str(&String::from_utf8(res)?)?;
    Ok(res)
}
pub async fn create_post(real_url: Url) -> Result<Post> {
    let created = created();
    let id = uuid::Uuid::new_v4();
    let mut title = None;
    let mut word_count = None;
    //let MercuryReturn { title, word_count } = parse(&real_url).context("NoneError")?;
    match parser::parse(&real_url).await {
        Ok(output) => {
            title = output.title;
            word_count = output.word_count;
        }
        Err(e) => {
            info!("mercury error! {:#?}", e);
        }
    };
    let post = Post::builder()
        .id(id)
        .title(title)
        .word_count(word_count)
        .real_url(real_url.to_string())
        .created(created)
        .build();
    Ok(post)
}
async fn user_exists(user_id: UserId, db: &Database) -> Result<Uuid> {
    let coll = db.collection::<User>("users");
    let user_id = to_bson(&user_id)?;
    let match_aggr = doc! {
    "$match":{
        "telegram_id": user_id
    }
    };
    let proj_aggr = doc! {
    "$project":{
    "id":1_i32,
    }
    };
    let pipeline = [match_aggr, proj_aggr];
    let vec_res = coll
        .aggregate(pipeline, None)
        .await?
        .next()
        .await
        .context("NoneError")??;
    let vec_res = from_document::<MongoRes>(vec_res)?;
    let res = vec_res.id;
    Ok(res)
}

use mongodb::bson::serde_helpers::uuid_as_binary;
#[derive(Debug, Deserialize, Serialize)]
struct MongoRes {
    _id: ObjectId,
    #[serde(with = "uuid_as_binary")]
    id: Uuid,
}
pub async fn create_user(
    telegram_id: UserId,
    telegram_username: Option<String>,
    _db: &Database,
) -> Result<Uuid> {
    let joined = chrono::Utc::now().into();
    let show_real_url = false;
    let id = uuid::Uuid::new_v4();
    let user = User::builder()
        .id(id)
        .telegram_id(telegram_id)
        .telegram_username(telegram_username)
        .joined(joined)
        .show_real_url(show_real_url)
        .build();
    let db = crate::db::connect_to_db().await?;
    let coll = db.collection::<Document>("users");
    coll.insert_one(to_document(&user)?, None).await?;
    Ok(id)
}
